const current = document.querySelector('#current') // full image
const imgs = document.querySelectorAll('.imgs img') // thumbnails
const opacity = .4 // image  thumbnail opacity
// with destructuring
// const [current, imgs] = [ document.querySelector('#current'), document.querySelectorAll('.imgs img')]

// listeners
imgs.forEach(img => img.addEventListener('click', imgClick))

// Set first image opacity on first load
imgs[0].style.opacity = opacity

/**
 * Change current image to clicked image
 * 
 * @param {MouseEvent} e
 */
function imgClick(e) {
	// Reset the opacity
	imgs.forEach(img => (img.style.opacity = 1))

	// Change current image to src of clicked image
	current.src = e.target.src

	// Add fade in class
	current.classList.add('fade-in')

	// Remove fade in class
	setTimeout(() => current.classList.remove('fade-in'), 500)

	// Change the opacity to opacity var
	e.target.style.opacity = opacity
}
